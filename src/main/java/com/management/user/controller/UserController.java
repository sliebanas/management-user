package com.management.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.user.model.UserDTO;
import com.management.user.model.UserUpdateDTO;
import com.management.user.service.UserServiceImpl;


@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserServiceImpl userServiceImpl;

	@GetMapping("/name")
	public String getUserMicroserviceName() {
		return "Microservicio de usuario";
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getUser(@PathVariable String id) {
		
		UserDTO userDTO = userServiceImpl.getUser(id);

		if(userDTO == null) {
			return new ResponseEntity<>("No existe un Usuario con ese id", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(userDTO, HttpStatus.OK);
	}
	
	
	@GetMapping("/users")
	public ResponseEntity<List<UserDTO>> getListUser() {
		
		List<UserDTO> listUsers = userServiceImpl.getListUsers();
		
		return new ResponseEntity<>(listUsers, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<String> createUser(@Valid @RequestBody UserDTO userDTO, BindingResult errors) {
		
		if(errors.hasErrors()) {
			return new ResponseEntity<>("No se puede crear el usuario", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		String userCreate = userServiceImpl.createUser(userDTO);
		
		return new ResponseEntity<>(userCreate, HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<String> updateUser(@PathVariable String id, @RequestBody UserUpdateDTO userUpdateDTO) {
		
		String userUpdate = userServiceImpl.updateUser(id, userUpdateDTO);
		
		if(userUpdate == null) {
			return new ResponseEntity<>(userUpdate, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(userUpdate, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable String id) {
		String erased = userServiceImpl.deleteUser(id);
		return new ResponseEntity<>(erased, HttpStatus.OK);
	}

}
