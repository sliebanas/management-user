package com.management.user.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserUtils {

	private final static Logger logger = LoggerFactory.getLogger(UserUtils.class);
	
	@Autowired
	private DNIValidator dniValidator;

	/**
	 * 
	 * Método que genera un id aleatorio en formato String, mediante SecureRandom
	 * 
	 * @return idUser random
	 */
	public String generatedRandomIdByUser(String dni) {
		String idGenerated = null;
		try {
			int secureRandom = Math.abs(SecureRandom.getInstanceStrong().nextInt());
			idGenerated = String.valueOf(secureRandom).toString().replaceAll("-", "").toUpperCase() + "-" + dni.toUpperCase();
			logger.debug("Se ha generado correctamente el idUser: " + idGenerated);
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return idGenerated;
	}

	/**
	 * Método que comprueba el tipo de id para buscar un User
	 * 
	 * @param id
	 * @return true si es con DNI, false si es con idUser
	 */
	public Boolean checkId(String id) {
		boolean type = false;
		
		int result = id.indexOf("-");
			if(result != -1) {
				type = true;
			}
		
		return type;
	}
	
	public boolean checkDniValidate(String dni) {
		boolean checkDni = dniValidator.validar(dni);
		
		return checkDni;
	}

}
