package com.management.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.management.user.entity.User;

public interface UserRepository extends JpaRepository<User, String> {

	Optional<User> findByDni(String dni);
	boolean existsByDni(String dni);
}
