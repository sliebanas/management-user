package com.management.user.mapper;

import org.springframework.stereotype.Component;

import com.management.user.entity.User;
import com.management.user.model.UserDTO;
import com.management.user.model.UserUpdateDTO;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
public class UserMapper extends ConfigurableMapper {

	protected void configure(MapperFactory factory) {

		factory.classMap(User.class, UserDTO.class)
		.byDefault()
		.register();
		
		factory.classMap(UserDTO.class, User.class)
		.byDefault()
		.register();

		factory.classMap(User.class, UserUpdateDTO.class)
		.byDefault()
		.register();
		
		factory.classMap(UserUpdateDTO.class, User.class)
		.byDefault()
		.register();
		
		factory.classMap(UserDTO.class, UserUpdateDTO.class)
		.byDefault()
		.register();

		factory.classMap(UserUpdateDTO.class, UserDTO.class)
		.byDefault()
		.register();
		
	}
}
