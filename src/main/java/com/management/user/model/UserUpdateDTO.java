package com.management.user.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Component
public class UserUpdateDTO implements Serializable {

	private static final long serialVersionUID = -6287390329869422309L;
	
	private String name;
	private String longname;
	private int age;
	private String dni;
}
