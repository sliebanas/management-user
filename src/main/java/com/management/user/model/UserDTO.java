package com.management.user.model;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.management.user.valid.ValidatorDNI;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Component
public class UserDTO implements Serializable {

	private static final long serialVersionUID = -630059166520433736L;

	@NotNull(message = "nombre.vacio")
	@Size(min = 2, max = 50, message = "nombre.size")
	private String name;
	
	@NotNull(message = "apellido.vacio")
	@Size(min = 2, max = 100, message = "apellido.size")
	private String longname;
	
	@NotNull(message = "edad.vacio")
	@Min(18)
	@Max(110)
	private int age;
	
	@NotNull(message = "dni.vacio")
	@ValidatorDNI
	private String dni;
}
