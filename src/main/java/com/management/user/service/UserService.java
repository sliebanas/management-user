package com.management.user.service;

import java.util.List;

import com.management.user.model.UserDTO;
import com.management.user.model.UserUpdateDTO;

public interface UserService {

	UserDTO getUser(String idUser);
	List<UserDTO> getListUsers();
	String createUser(UserDTO userDTO);
	String updateUser(String id, UserUpdateDTO userUpdateDTO);
	String deleteUser(String idUser);
}
