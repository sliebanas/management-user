package com.management.user.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.management.user.entity.User;
import com.management.user.mapper.UserMapper;
import com.management.user.model.UserDTO;
import com.management.user.model.UserUpdateDTO;
import com.management.user.repository.UserRepository;
import com.management.user.utils.UserUtils;

@Service
public class UserServiceImpl implements UserService {

	private final static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserUtils userUtils;

	@Autowired
	private UserMapper userMapper;


	@Override
	public UserDTO getUser(String id) {

		User user = getUserByType(id);

		if(user != null) {
			UserDTO userDTO = userMapper.map(user, UserDTO.class);
			return userDTO;
		}

		return null;
	}

	@Override
	public List<UserDTO> getListUsers() {

		logger.info("Entrada al servicio de listar usuarios");
		List<User> listUsers = userRepository.findAll();

		if(listUsers == null || listUsers.size() == 0) {
			logger.info("El listado de usuarios se ha obtenido vacío.");
			return new ArrayList<UserDTO>();
		}

		logger.info("Listado de usuarios con datos");
		return userMapper.mapAsList(listUsers, UserDTO.class);
	}

	@Override
	public String createUser(UserDTO userDTO) {

		//Comprobamos que el DNI no exista ya en la base de datos
		boolean checkExistDNI = userRepository.existsByDni(userDTO.getDni());

		if(checkExistDNI) {
			return "El DNI insertado ya existe";
		}

		logger.info("Entrada al servicio de listar usuarios");
		User user = userMapper.map(userDTO, User.class);
		user.setIdUser(userUtils.generatedRandomIdByUser(user.getDni()));

		try {
			logger.info("Se va a proceder a guardar el usuario: " + user.getIdUser());
			userRepository.save(user);
		} catch (Exception e) {
			logger.error("Error al guardar el usuario: " + e.getMessage());
			return null;
		}

		logger.info("Usuario creado correctamente");
		return "Usuario creado correctamente";
	}

	@Override
	public String updateUser(String id, UserUpdateDTO userUpdateDTO) {

		if(userUpdateDTO == null) {
			return null;
		}

		User user = getUserByType(id);

		if(user == null) {
			return null;
		}

		String updated = toUpdateUser(user, userUpdateDTO);

		return updated;
	}

	@Override
	public String deleteUser(String id) {

		User user = getUserByType(id);

		if(user != null) {
			try {
				logger.info("Usuario a borrar con id: " + user.getIdUser());
				userRepository.delete(user);
				logger.info("Usuario borrado correctamente");
				return "Usuario borrado correctamente";
			} catch (Exception e) {		
				logger.error("Error en el proceso de borrado del Usuario");
				logger.error(e.getMessage());
				return "Usuario borrado correctamente";
			}
		}

		return "El id introducido no corresponde a ningún usuario. No se puede eliminar el Usuario.";
	}

	/**
	 * Método para obtener el Usuario dependiendo del tipo de id introducido
	 * 
	 * @param id. Tipo idUser o DNI.
	 * @return User
	 */
	private User getUserByType(String id) {
		boolean typeId = userUtils.checkId(id);
		User user = null;

		if(typeId) {
			user = userRepository.findById(id).orElse(null);
		} else {
			user = userRepository.findByDni(id).orElse(null);
		}

		return user;
	}

	/**
	 * Método de actualización del Usuario
	 * 
	 * @param user
	 * @param userUpdateDTO
	 * @return User update
	 */
	private String toUpdateUser(User user, UserUpdateDTO userUpdateDTO) {

		if(userUpdateDTO.getName() != null && !userUpdateDTO.getName().isEmpty() && !userUpdateDTO.getName().equals(user.getName())) {
			user.setName(userUpdateDTO.getName());
		}

		if(userUpdateDTO.getLongname() != null && !userUpdateDTO.getLongname().isEmpty() && !userUpdateDTO.getLongname().equals(user.getLongname())) {
			user.setLongname(userUpdateDTO.getLongname());
		}

		if(userUpdateDTO.getAge() != user.getAge()) {
			user.setAge(userUpdateDTO.getAge());
		}

		if(userUpdateDTO.getDni() != null && !userUpdateDTO.getDni().isEmpty() && !userUpdateDTO.getDni().equalsIgnoreCase(user.getDni())) {
			boolean dniValidate = userUtils.checkDniValidate(userUpdateDTO.getDni());
			
			if(!dniValidate) {
				logger.error("El DNI introducido no es correcto: " + userUpdateDTO.getDni());
				return "Fallo al actualziar el Usuario. DNI incorrecto.";
			}
				
			user.setDni(userUpdateDTO.getDni().toUpperCase());
		}

		try {
			userRepository.save(user);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "Fallo al actualziar el Usuario";
		}

		return "Usuario actualizado";
	}

}
