package com.management.user.valid;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.management.user.utils.DNIValidator;

@Documented
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DNIValidator.class)
public @interface ValidatorDNI {

	String message() default "{Dni}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
