package com.management.user.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class User {

	@Id
	private String idUser;
	@NotNull
	private String name;
	@NotNull
	private String longname;
	@NotNull
	private int age;
	@NotNull
	private String dni;
}
